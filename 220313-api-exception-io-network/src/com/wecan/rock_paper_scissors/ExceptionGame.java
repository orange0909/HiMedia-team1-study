package com.wecan.rock_paper_scissors;

public class ExceptionGame {
	public static void exceptionGame() {
		int random = (int)(Math.random() * 3);
		
		String result = (random == 0) ? "가위" : (random == 1) ? "바위" : "보";
		
		switch(result) {
			case "가위": throw new ArithmeticException();
			case "바위": throw new IndexOutOfBoundsException();
			case "보": throw new ClassCastException();
		}
	}
}
