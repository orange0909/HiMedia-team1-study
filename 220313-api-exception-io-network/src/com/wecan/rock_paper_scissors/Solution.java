package com.wecan.rock_paper_scissors;

public class Solution {
	public static void solution() {
		try {
			ExceptionGame.exceptionGame();
		} catch(ArithmeticException e) {
			System.out.println("가위를 내셨군요?");
		} catch(IndexOutOfBoundsException e) {
			System.out.println("바위를 내셨군요?");
		} catch(ClassCastException e) {
			System.out.println("보를 내셨군요?");
		}
	}
}
