package com.wecan.string_length;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("최대 문자열 길이를 입력하세요: ");
		int maxLength = sc.nextInt();
		int remain = maxLength;
		String result = "";
		
		while(remain > 0) {
			System.out.print("문자열을 입력하세요: ");
			String str = sc.next();
			int length = str.length();
			
			if(remain > length) {		// 입력 받은 문자열보다 저장할 수 있는 공간이 큰 경우  
				result += str;
				remain -= length;
			} else {
				System.out.println("최대 문자 갯수를 넘었습니다.");
				result += str.substring(0, remain);
				remain = 0;
			}
		}
		
		System.out.println(result);
		sc.close();
	}
}
