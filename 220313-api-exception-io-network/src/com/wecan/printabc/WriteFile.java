package com.wecan.printabc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {
	public static void writerFile() {
		FileWriter writer = null;
		
		try {
			File file = new File("src/com/wecan/printabc/File.txt");
			file.createNewFile();
			
			writer = new FileWriter(file);
			
			char[] chArray = CharArray.charArray(); 
			
			for(char ch : chArray) {
				writer.write(ch);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
