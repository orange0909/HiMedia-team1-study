package com.wecan.printabc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {
	public static void readFile() {
		FileReader reader = null;
		
		try {
			reader = new FileReader("src/com/wecan/printabc/File.txt");
			
			int ch = ' ';
			while(true) {
				ch = reader.read();
				
				if(ch == -1) {
					break;
				}
				
				System.out.print((char)ch);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
