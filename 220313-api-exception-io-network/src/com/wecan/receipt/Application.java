package com.wecan.receipt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		final int MILK_PRICE = 2000;
		final int ONION_PRICE = 1500;
		final int CARROT_PRICE = 700;
		final int GREEN_ONION_PRICE = 1200;
		final int EGGS_PRICE = 5000;
		
		Scanner sc = new Scanner(System.in);
		PrintWriter pw = null;
		
		try {
			pw = new PrintWriter(new FileOutputStream(new File("src/com/wecan/receipt/receipt.txt")));
			
			int sum = 0;
			
			while(true) {
				System.out.print("구입할 물건을 입력하세요: ");
				String choice = sc.next();
				int price = 0;
				
				switch(choice) {
					case "우유":
						price = MILK_PRICE;
						break;
					case "양파":
						price = ONION_PRICE;
						break;
					case "당근":
						price = CARROT_PRICE;
						break;
					case "대파":
						price = GREEN_ONION_PRICE;
						break;
					case "계란":
						price = EGGS_PRICE;
						break;
					case "종료":
						System.out.println("안녕히가세요");
						pw.println("총액 " + sum + "원");
						pw.flush();
						return;
					default:
						System.out.println(choice + "은(는) 구입할 수 없습니다.");
						continue;
				}
				
				/* switch에서 default, "종료" 케이스로는 도달할 수 없음 */
				System.out.println(choice + "을(를) 구입했습니다.");
				sum += price;
				pw.println(choice + " " + price + "원");
				pw.flush();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			pw.close();
			sc.close();
		}
	}
}
