package com.wecan.corona_count;

public class Person {
	String name;
	static int numOfCovid = 0;
	boolean isCovid = false;
	
	public Person(String name) {
		this.name = name;
	}
	
	public static void printNumOfCovid() {
		System.out.println("현재 확진자 수는 " + Person.numOfCovid + "명입니다.");
	}
	
	public void getCovid() {
		if(!this.isCovid) {
			System.out.println(this.name + "님이 코로나에 걸렸습니다.");
			this.isCovid = true;
			Person.numOfCovid++;
		} else {
			System.out.println("이미 걸린 사람은 또 걸릴 수 없습니다.");
		}
	}
	
	public void takeMedicine() {
		if(this.isCovid) {
			System.out.println(this.name + "님의 코로나가 완치되었습니다.");
			this.isCovid = false;
			Person.numOfCovid--;
		} else {
			System.out.println("아무 일도 일어나지 않았습니다.");
		}
	}
	
	@Override
	public String toString() {
		if(this.isCovid) {
			return this.name + "님은 코로나 환자입니다.";
		} else {
			return this.name + "님은 코로나 환자가 아닙니다.";
		}
	}
}
