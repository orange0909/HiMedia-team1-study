package com.wecan.corona_count;

public class Application {
	public static void main(String[] args) {
		Person p1 = new Person("철수");
		Person p2 = new Person("수지");
		
		p1.getCovid();
		p2.getCovid();
		
		p1.takeMedicine();
		
		System.out.println(p1);
		System.out.println(p2);
		
		Person.printNumOfCovid();
	}
}
