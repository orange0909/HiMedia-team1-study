package com.wecan.makeVehicle;

public class Vehicle {
	private String kinds;
	private int numberOfTire;
	
	public Vehicle(int numberOfTire) {
		switch(numberOfTire) {
			case 2:
				this.numberOfTire = numberOfTire;
				this.kinds = "오토바이";
				break;
			case 3:
				this.numberOfTire = numberOfTire;
				this.kinds = "세발자전거";
				break;
			case 4:
				this.numberOfTire = numberOfTire;
				this.kinds = "자동차";
				break;
			default:
				System.out.println("잘못된 입력입니다.");
				return;
		}
	}
	public Vehicle(String kinds) {
		switch(kinds) {
			case "오토바이":
				this.numberOfTire = 2;
				this.kinds = kinds;
				break;
			case "세발자전거":
				this.numberOfTire = 3;
				this.kinds = kinds;
				break;
			case "자동차":
				this.numberOfTire = 4;
				this.kinds = kinds;
				break;
			default:
				System.out.println("잘못된 입력입니다.");
				return;
		}
	}
	
	@Override
	public String toString() {
		return "바퀴 갯수가 " + this.numberOfTire + "개인 " + this.kinds + "입니다.";
	}
}
