package com.wecan.makeVehicle;

public class Application {
	public static void main(String[] args) {
		Vehicle vh1 = new Vehicle(2);
		Vehicle vh2 = new Vehicle("세발자전거");
		Vehicle vh3 = new Vehicle(4);

		System.out.println(vh1);
		System.out.println(vh2);
		System.out.println(vh3);
	}
}
