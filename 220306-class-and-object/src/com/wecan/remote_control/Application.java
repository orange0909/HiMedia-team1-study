package com.wecan.remote_control;

public class Application {
	public static void main(String[] args) {
		/*
		 * Application의 main을 채우자.
		 * Remote[] 타입을 갖는 TotalRemote 레퍼런스 변수에 Light1 ~ 3의 인스턴스를 넣어서 한 번에 전등을 켜보자.
		 * 
		 * Hint = Remote[] / for(Remote r: TotalRemote) {r;}
		 * 
		 * Light1과 Light2만 전등을 꺼보자. 
		 * Hint = if(Remote1 instanceof Light1) / if (Remote1 instanceof Light1) / 
		 * 
		 */
		
		Remote[] totalRemote = { new Light1(), new Light2(), new Light3() };
		
		for(Remote r : totalRemote) {
			r.turnOnTheLight();
		}
		
		for(Remote r : totalRemote) {
			if(!(r instanceof Light3)) {
				r.turnOffTheLight();
			}
		}
	}
}
