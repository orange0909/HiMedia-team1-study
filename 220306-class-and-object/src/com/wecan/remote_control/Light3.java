package com.wecan.remote_control;

public class Light3 extends Remote {
	@Override
	public void turnOnTheLight() {
		System.out.println("Light3이(가) 켜졌습니다.");
	}
	@Override
	public void turnOffTheLight() {
		System.out.println("Light3이(가) 꺼졌습니다.");
	}
}
