package com.wecan.remote_control;

public class Light1 extends Remote {
	@Override
	public void turnOnTheLight() {
		System.out.println("Light1이(가) 켜졌습니다.");
	}
	@Override
	public void turnOffTheLight() {
		System.out.println("Light1이(가) 꺼졌습니다.");
	}
}
