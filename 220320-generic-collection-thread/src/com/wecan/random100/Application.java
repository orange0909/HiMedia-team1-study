package com.wecan.random100;

import java.util.ArrayList;
import java.util.List;

public class Application {
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		
		for(int i = 0; i < 100; i++) {
			int random = (int)(Math.random() * 1000) + 1;
			
			list.add(random);
		}
		
		list.sort(new DescendingNumber());
		
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + "\t");
			
			if((i + 1) % 10 == 0) {
				System.out.println();
			}
		}
	}
}
