package com.wecan.using_generic;

public class Application {
	public static void main(String[] args) {
		GenericClass<String> gc1 = new GenericClass<String>("String");
		GenericClass<Boolean> gc2 = new GenericClass<Boolean>(true);
		GenericClass<Integer> gc3 = new GenericClass<Integer>(123);
		GenericClass<Double> gc4 = new GenericClass<Double>(123.4);
		
		gc1.myType();
		gc2.myType();
		gc3.myType();
		gc4.myType();
		
	}
}
