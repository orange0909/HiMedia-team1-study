package com.wecan.using_generic;

public class GenericClass<T> {
	T value;
	
	public GenericClass(T value) {
		this.value = value;
	}
	
	public void myType() {	
		if(value instanceof String) {
			System.out.println("String 입니다!!");
		} else if(value instanceof Integer ) {
			System.out.println("Integer 입니다!!");
		} else if(value instanceof Boolean) {
			System.out.println("Boolean 입니다!!");
		} else {
			System.out.println("잘 모르겠습니다!!");
		}
	}
}
