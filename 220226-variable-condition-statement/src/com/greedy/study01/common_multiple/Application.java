package com.greedy.study01.common_multiple;

public class Application {
	public static void main(String[] args) {
		
		/* 
		 * for문과 continue문을 사용하여 1부터 100까지의 수 중에
		 * 3과 5의 배수를 출력하지만 3의 배수이면서 5의 배수인 경우는
		 * 출력하지 않는 프로그램을 작성하시오
		 * ex) 2와 7의 경우는
		 * 2	4	6	7	8	10	12	16 --> 이런식으로 공배수는 빼고 출력
		 */
		
		for(int i = 1; i <=100; i++) {
			if(i % 3 == 0 && i % 5 == 0) {
				continue;
			}
			
			if(i % 3 == 0 || i % 5 == 0) {
				System.out.print(i + " ");
			}
		}
	}
}
