package com.greedy.study01.vending_machine;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		/* 기본 정보 초기화 */
		long money = 2000;
		long colaPrice = 1500;
		long ciderPrice = 1300;
		long juicePrice = 2000;
		
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("소지금 : " + money + "원");
			
			/* 메뉴 출력 */
			System.out.println("1. 콜라 : 1500원");
			System.out.println("2. 사이다 : 1300원");
			System.out.println("3. 오렌지 주스 : 2000원");
			
			/* 입력 */
			System.out.print("음료를 선택하세요 : ");
			int choiceNum = sc.nextInt();
			
			String choiceDrink = "";
			long price = 0;
			
			/* 입력에 대한 분기 */
			switch(choiceNum) {
				case 1:
					choiceDrink = "콜라";
					price = colaPrice;
					break;
				case 2:
					choiceDrink = "사이다";
					price = ciderPrice;
					break;
				case 3:
					choiceDrink = "오렌지 주스";
					price = juicePrice;
					break;
				case -1:
					System.out.println("종료합니다.");
					return;
				default:
					System.out.println("잘못 선택하셨습니다.");
					continue;
			}
			
			/* 소지금 검증 및 음료 제공 */
			if(money >= price) {
				money -= price;
				System.out.println(choiceDrink + "가 나왔습니다!");
			} else {
				System.out.println("소지금이 부족합니다.");
			}
		}
	}
}
