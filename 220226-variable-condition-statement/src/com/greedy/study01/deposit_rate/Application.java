package com.greedy.study01.deposit_rate;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		
		/*
		 * 먼저 초기 금액을 입력 받는다.
		 * 금액에 따라 이율이 다를 때 1년 뒤에 어떻게 변하는지 계산하여 출력한다.
		 */
		
		/* 
		 * 10000 보다 적을 때 한 달에 5%의 이율
		 * 10000 보다 클 때 한 달에 10%의 이율
		 * 
		 * 한 달 뒤의 금액 = 기존 금액 * (1 + 이율)
		 * 두 달 뒤의 금액 = 한 달 뒤의 금액 * (1 + 이율)
		 * ...
		 * 
		 * 입력
		 * "기존 금액을 입력하세요 : "
		 * 
		 * 출력
		 * "일 년 뒤의 금액은 ~~입니다."
		 */
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("기존 금액을 입력하세요 : ");
		int deposit = sc.nextInt();
		
		double depositRate = 0.0;
		
		if(deposit >= 10000) {
			depositRate = 0.1;
		} else {
			depositRate = 0.05;
		}
		
		for(int i = 0; i < 12; i++) {
			deposit *= 1 + depositRate;
		}
		
		System.out.println("일년 뒤의 금액은 " + deposit + "원 입니다.");
	}
}
