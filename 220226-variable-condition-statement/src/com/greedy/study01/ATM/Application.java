package com.greedy.study01.ATM;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		long cash = 10000000L;
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("현재 보유 현금 : " + cash);
			System.out.print("출금할 금액을 입력하세요 : ");
			
			long withdrawal = sc.nextLong();
			
			if(withdrawal == -1) {
				System.out.println("종료합니다.");
				return;
			}
			
			if(cash >= withdrawal) {			// ATM의 현금보다 출금량이 작음
				cash -= withdrawal;
				System.out.println(withdrawal + "원이 출금 되었습니다.");
			} else {
				System.out.println("현금이 부족합니다.");
			}
		}
	}
}
