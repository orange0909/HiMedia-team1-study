package com.greedy.study01.have_one;

public class Application {
	public static void main(String[] args) {
		
		/* haveOne의 값에 숫자 '1'의 포함여부를 판별하세요.
		 * (for문 / if문(return) / input.charAt(i) 사용)
		 * 사용된 변수명 : haveOne
		 * 
		 * answer : "haveOne에 숫자 1이 있습니다."
		 */
		
		String haveOne = "abcdefg";
		
		for(int i = 0; i < haveOne.length(); i++) {
			if(haveOne.charAt(i) == '1') {
				System.out.println("haveOne에 숫자 1이 있습니다");
				return;
			}
		}
	}
}
