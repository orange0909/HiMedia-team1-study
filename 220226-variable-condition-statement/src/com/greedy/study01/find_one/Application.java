package com.greedy.study01.find_one;

public class Application {
	public static void main(String[] args) {
		
		/*
		 * 다음 주어진 2차원 배열에 있는 1의 갯수를 구하세요
		 * 이중for문 사용
		 * 사용된 변수명 : sumOfNumberOne
		 * 
		 * answer : howManyNumberInArray에 있는 1의 갯수는23개 입니다.
		 * 
		 */
		
		int[][] howManyNumberInArray = {
				{1, 3, 1, 5, 1, 3, 1, 5, 1},
				{6, 4, 1, 3, 1,},
				{1, 2, 1, 3, 7, 4 ,1 ,1 ,3},
				{1, 7, 8, 2, 1, 5, 2,},
				{6, 4, 1, 3, 1, 2, 1, 2, 1, 1, 3},
				{1, 7, 8, 2, 1, 0},
				{0, 1, 0, 1, 7, 8, 2, 1, 4}
		};
		
		int sumOfNumberOne = 0;
		
		for(int i = 0; i < howManyNumberInArray.length; i++) {
			for(int j = 0; j < howManyNumberInArray[i].length; j++) {
				if(howManyNumberInArray[i][j] == 1) {
					sumOfNumberOne++;
				}
			}
		}
		
		System.out.println("howManyNumberInArray에 있는 1의 갯수는 " + sumOfNumberOne + "개 입니다.");
	}
}
