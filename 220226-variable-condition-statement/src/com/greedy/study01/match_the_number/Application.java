package com.greedy.study01.match_the_number;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		int computerAnswer = (int)(Math.random() * 10) + 1;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("1 ~ 10 중 답이라고 생각되는 정수를 입력하세요.");
		
		while(true) {
			int myAnswer = sc.nextInt();
			
			if(computerAnswer < myAnswer) {
				System.out.println("더 낮은 수를 입력하세요");
			} else if (computerAnswer > myAnswer) {
				System.out.println("더 높은 수를 입력하세요");
			} else {
				System.out.println("정답입니다.");
				return;
			}
		}
	}
}
